/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.laboxoop;

/**
 *
 * @author informatics
 */
public class Player {
    private char symbol;
    private int winCount, loseCount, DrawCount;

    public Player(char symbol) {
        this.symbol = symbol;
        this.winCount = 0;
        this.loseCount = 0;
        this.DrawCount = 0;
    }
    
    public void win() {
        winCount++;
    }
    
    public void lose() {
        loseCount++;
    }
    
    public void draw() {
        DrawCount++;
    }
    public char getSymbol() {
        return symbol;
    }

    public int getWinCount() {
        return winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public int getDrawCount() {
        return DrawCount;
    }
    
}
